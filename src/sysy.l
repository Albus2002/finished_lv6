%option noyywrap
%option nounput
%option noinput

%{

#include <cstdlib>
#include <string>
#include <iostream>
#include "ast.h"
// 因为 Flex 会用到 Bison 中关于 token 的定义
// 所以需要 include Bison 生成的头文件
#include "sysy.tab.hpp"

using namespace std;

%}

/* 空白符和注释 */
WhiteSpace    [ \t\n\r]*
LineComment   "//".*
LongComment   "/*"([^\*]|(\*)*[^\*/])*(\*)*"*/"

/* 标识符 */
Identifier    [a-zA-Z_][a-zA-Z0-9_]*

/* 整数字面量 */
Decimal       [1-9][0-9]*
Octal         0[0-7]*
Hexadecimal   0[xX][0-9a-fA-F]+

/*比较符号*/
LessEqual       "<="
GreaterEqual    ">="
Less            "<"
Greater         ">"
Equal           "=="
NotEqual        "!="

/*逻辑运算符*/
And             "&&"
Or              "||"

/*分支与循环*/

%%

{WhiteSpace}    { /* 忽略, 不做任何操作 */ }
{LineComment}   { /* 忽略, 不做任何操作 */ }
{LongComment}   { /* 忽略, 不做任何操作 */ }

"int"           { return INT; }
"return"        { return RETURN; }
"const"         { return CONST; }
"if"            { return IF; }
"else"          { return ELSE; }


{Identifier}    { yylval.str_val = new string(yytext); return IDENT; }

{Decimal}       { yylval.int_val = strtol(yytext, nullptr, 0); return INT_CONST; }
{Octal}         { yylval.int_val = strtol(yytext, nullptr, 0); return INT_CONST; }
{Hexadecimal}   { yylval.int_val = strtol(yytext, nullptr, 0); return INT_CONST; }

{LessEqual}     {return LE;}
{GreaterEqual}  {return GE;}
{Equal}         {return EQ;}
{NotEqual}      {return NE;}
{Less}          {return LT;}
{Greater}       {return GT;}
{And}           {return AND;}
{Or}            {return OR;}

.               { return yytext[0]; }

%%
