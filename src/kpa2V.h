#pragma once
#include "ast.h"
#include "util.h"
#include<iostream>
#include<cstring>
#include<map>

using namespace std;

static map<koopa_raw_value_t,R_code> value_reg_mp;
static int global_var_cnt = 0;
static int global_var_id = -100000;

static const char* riscv_label(const char*label){
  return (const char*)(label+1);//跳过%
}

static string load_reg(string reg,int stack_bio,const R_Info& info){
  if(stack_bio > (1 << 30)){
    cout<<"error of stack explosion: "<< stack_bio << endl;
    assert(0);
  }
  if(stack_bio <= global_var_id){
    int global_cnt = global_var_id - stack_bio;
    string tmp = "  la " + reg + ", globalVar" + to_string(global_cnt) + "\n";
    return tmp;
  }
  else if(stack_bio < 2048 && stack_bio >= -2048){
    return "  lw " + reg + ", " + to_string(stack_bio) + "(sp)\n";
  }
  else{
    string tmp = "  li t6, " + to_string(stack_bio) + "\n";
    tmp.append("  add t6, sp, t6\n");
    tmp.append("  lw "+ reg + ", 0(t6)\n");
    return tmp;
  }
}

// 函数声明
R_code Visit(const koopa_raw_program_t& program);
R_code Visit(const koopa_raw_slice_t &slice,const R_Info& info);
R_code Visit(const koopa_raw_function_t &func,const R_Info& info);
R_code Visit(const koopa_raw_basic_block_t &bb,const R_Info& info);
R_code Visit(const koopa_raw_value_t &value,const R_Info& info);
R_code Visit(const koopa_raw_return_t& ret,const R_Info& info);
R_code Visit(const koopa_raw_integer_t& integer, const R_Info& info);
R_code Visit(const koopa_raw_jump_t& jump,const R_Info& info);
R_code Visit(const koopa_raw_store_t& store,const R_Info& info);
R_code Visit(const koopa_raw_global_alloc_t& alloc,const R_Info& info);
R_code Visit(const koopa_raw_branch_t& branch,const R_Info& info);
R_code Visit(const koopa_raw_binary_t& binary,const R_Info& info);
R_code Visit(const koopa_raw_load_t& load,const R_Info& info);

static int get_basic_block_size(const koopa_raw_basic_block_t& basic_block);
static int get_ins_size(const koopa_raw_value_t& value);
static int get_func_size(const koopa_raw_function_t& func);
static int get_func_arg_size(const koopa_raw_function_t& func);
static string store_reg(string reg, int stack_bio, const R_Info& info);
string KIR_RISCV(const char* str);

static int get_basic_block_size(const koopa_raw_basic_block_t& basic_block){
  int size = 0;
  auto instruction = basic_block->insts;
  for(int i=0;i<instruction.len;++i){
    auto inst = (koopa_raw_value_t)instruction.buffer[i];
    int inst_size = get_ins_size(inst);
    size += inst_size;
  }
  return size;
}

static int get_ins_size(const koopa_raw_value_t& value){
  switch (value->kind.tag) {
        case KOOPA_RVT_ALLOC: {
            return 4 + 4;
        }
        case KOOPA_RVT_GLOBAL_ALLOC: {
            return 4;
        }
        case KOOPA_RVT_LOAD: {
            return 4;
        }
        case KOOPA_RVT_STORE: {
            return 8;
        }
        case KOOPA_RVT_GET_ELEM_PTR: {
            return 8;
        }
        case KOOPA_RVT_GET_PTR: {
            return 8;
        }
        case KOOPA_RVT_BINARY: {
            return 8;
        }
        case KOOPA_RVT_BRANCH: {
            return 4;
        }
        case KOOPA_RVT_JUMP: {
            return 4;
        }
        case KOOPA_RVT_RETURN: {
            return 4;
        }
        case KOOPA_RVT_CALL: {
            return 4;
        }
        default: {
            if (debug)
                cout << "error type: " << value->kind.tag << endl;
            assert(false);
        }
    }
}

static int get_func_size(const koopa_raw_function_t& func){
  int size = 4;
  size += 8;
  auto bbs = func->bbs;
  for(int i=0;i<bbs.len;++i){
    size+=get_basic_block_size((koopa_raw_basic_block_t)bbs.buffer[i]);
  }
  size += get_func_arg_size(func);
  size = 16 * ((size+15)/16);
  return size;
}
static int get_func_arg_size(const koopa_raw_function_t& func){
  int max_arg_len = 0;
    auto bbs = func->bbs;
    for (int i = 0; i < bbs.len; i++) {
        auto bb = (koopa_raw_basic_block_t)bbs.buffer[i];
        auto insts = bb->insts;
        for (int j = 0; j < insts.len; j++) {
            auto inst = (koopa_raw_value_t)insts.buffer[j]; //取出每个指令
            if (inst->kind.tag == KOOPA_RVT_CALL) {
                auto inst_call = inst->kind.data.call;
                int arg_len = inst_call.args.len;
                max_arg_len =
                    max(max_arg_len, 4 * max(arg_len - 8, 0)); //最多8个参数，多余的参数放在栈中
            }
        }
    }
    return max_arg_len;
}

static string store_reg(string reg, int stack_bio, const R_Info& info){
  if (stack_bio > 1 << 30) {
        cout << "booooom: " << stack_bio << endl;
        assert(false);
    }
    if (stack_bio <= global_var_id) {
        if (debug)
            cout << "NO STORE: " + to_string(stack_bio) << endl;
        assert(false);
    }
    if (info.stack_size < 2048)
        return "  sw " + reg + ", " + to_string(stack_bio) + "(sp)\n";
    else {
        string code = "  li t6, " + to_string(stack_bio) + "\n";
        code.append("  add t6, sp, t6\n");
        code.append("  sw " + reg + ", 0(t6)\n");
        return code;
    }
}


// 访问 raw program
R_code Visit(const koopa_raw_program_t &program) {
  // 执行一些其他的必要操作
  // ...
  R_Info info;
  // 访问所有全局变量
  R_code values_code = Visit(program.values,info);
  if(debug){
      cout<< "global sucess\n";
    }
  // 访问所有函数
  R_code funcs_code = Visit(program.funcs,info);
  if(debug){
      cout<< "func sucess\n";
    }

  return R_code(values_code.code + funcs_code.code);
}

// 访问 raw slice
R_code Visit(const koopa_raw_slice_t &slice,const R_Info& info) {
  R_Info nextInfo = info;
  string ret = "";
  if(debug){
        cout<< "slice kind: "<<slice.kind<<endl;
    }
  for (size_t i = 0; i < slice.len; ++i) {
    auto ptr = slice.buffer[i];
    // 根据 slice 的 kind 决定将 ptr 视作何种元素
    switch (slice.kind) {
      case KOOPA_RSIK_FUNCTION:{// 访问函数
        if(debug){
                cout<< "func translate\n";
        }
        auto func_ptr = reinterpret_cast<koopa_raw_function_t>(ptr);
        if(func_ptr->bbs.len==0){
          break;
        }
        ret.append(Visit(func_ptr,nextInfo).code);
        break;
        }
      case KOOPA_RSIK_BASIC_BLOCK:{
        // 访问基本块
        if(debug){
                cout<< "bb translate\n";
        }
        auto block_ptr = reinterpret_cast<koopa_raw_basic_block_t>(ptr);
        ret.append(Visit(block_ptr,nextInfo).code);
        nextInfo.stack_bio += get_basic_block_size(block_ptr);
        break;
        }
      case KOOPA_RSIK_VALUE:{// 访问指令
        if(debug){
            cout<< "risc_val translate\n";
            cout<<"stack_bio :" << nextInfo.stack_bio << endl;
        }
        auto value_ptr = reinterpret_cast<koopa_raw_value_t>(ptr);
        ret.append(Visit(value_ptr,nextInfo).code);
        nextInfo.stack_bio += get_ins_size(value_ptr);
        break;
        }
      default:
        // 我们暂时不会遇到其他内容, 于是不对其做任何处理
        assert(false);
    }
  }
  return R_code(ret);
}

// 访问函数
R_code Visit(const koopa_raw_function_t &func,const R_Info& info) {
  // 执行一些其他的必要操作
  // ...
  string ret = "\n  .text\n  .globl ";
  R_Info nextInfo;
  const char* func_name = (const char*)(func->name + 1);
  //跳过开头的@
  ret.append(func_name);
  ret.append("\n");
  ret.append(func_name);
  ret.append(":\n");

  // 访问所有基本块
  if(debug){
        cout<< "bbs translate\n";
    }
  int func_size = get_func_size(func);
  nextInfo.stack_size = func_size;
  nextInfo.func_name = func_name;
  nextInfo.stack_bio = 4 + get_func_arg_size(func);
  nextInfo.func_param = func->params;
  ret.append(Visit(func->bbs,nextInfo).code);
  
  return R_code(ret);
}

// 访问基本块
R_code Visit(const koopa_raw_basic_block_t &bb,const R_Info& info) {
  // 执行一些其他的必要操作
  // ...
  const char* bname = bb->name;
  cout << bname <<endl;
  // 访问所有指令
  string ret = "";
  if(debug){
        cout<< "insts translate\n";
    }
  ret = Visit(bb->insts,info).code;
  if(strcmp(bb->name,"%entry")==0){
    string prefix = "";
    if (info.stack_size < 2048) {
        prefix += "  addi sp, sp, -" + to_string(info.stack_size) + "\n";
        //addi sp, sp, -2048
        prefix += "  sw ra, 0(sp)\n";
    } else {
        prefix += "  li t0, -" + to_string(info.stack_size) + "\n";
        //li t0, -2048
        prefix += "  add sp, sp, t0\n";
        prefix += "  sw ra, 0(sp)\n";
    }
    ret = prefix + ret;
  }
  else{
    string new_name = riscv_label(bname);
    ret = new_name + ":\n" + ret;
  }
  return R_code(ret);
}

// 访问指令
R_code Visit(const koopa_raw_value_t &value,const R_Info& info) {
  // 根据指令类型判断后续需要如何访问
  //每个指令只生成一次，其余的会直接返回
  if(value_reg_mp.find(value) != value_reg_mp.end()){
    if(debug){
      cout<< value->kind.tag << endl;
    }
    return value_reg_mp[value];
  }
  const auto &kind = value->kind;
  if(debug){
    cout<<"value kind: "<<kind.tag<<endl;
  }
  R_code tmp = R_code("");
  switch (kind.tag) {
    case KOOPA_RVT_RETURN:{// 访问 return 指令
      if(debug){
            cout<<"return cmd\n";
        }
      tmp = Visit(kind.data.ret,info);
      break;
    }
    case KOOPA_RVT_INTEGER:{// 访问 integer 指令
      if(debug){
            cout<<"integer cmd\n";
        }
      tmp = Visit(kind.data.integer,info);
      break;
    }
    case KOOPA_RVT_STORE: { //访问 store 指令
        if(debug){
                cout<<"store cmd\n";
        }
        tmp = Visit(kind.data.store,info);
        break;
    }
    case KOOPA_RVT_BINARY: { //访问 binary 指令
        if(debug){
                cout<<"binary cmd\n";
        }
        tmp = Visit(kind.data.binary,info);
        break;
    }
    case KOOPA_RVT_JUMP: { //访问 jump 指令
        if(debug){
                cout<<"jump cmd\n";
        }
        tmp = Visit(kind.data.jump,info);
        break;
    }
    case KOOPA_RVT_BRANCH: { //访问 branch 指令
        if(debug){
                cout<<"branch cmd\n";
        }
        tmp = Visit(kind.data.branch,info);
        break;
    }
    case KOOPA_RVT_LOAD: { //访问 load 指令
        if(debug){
                cout<<"load cmd\n";
        }
        tmp = Visit(kind.data.load,info);
        break;
    }
    case KOOPA_RVT_ALLOC: { //访问 alloc 指令
        if(debug){
                cout<<"alloc cmd\n";
        }
        tmp = Visit(kind.data.global_alloc,info);
        break;
    }
    default:
      // 其他类型暂时遇不到
      assert(false);
  }
  value_reg_mp[value] = tmp;
  return tmp;
}
//return
R_code Visit(const koopa_raw_return_t& ret,const R_Info& info){
    if(debug){
        cout<<"raw_return start.\n";
    }
    string s = "";
    if(ret.value != nullptr){
        R_code tmp = Visit(ret.value,info);
        if(tmp.append != 0) s += tmp.code;
        s += load_reg("a0",tmp.retAddr,info);
    }
    s += load_reg("ra",0,info);
    if(info.stack_size < 2048){
      s.append("  addi sp, sp, " + to_string(info.stack_size) + "\n");
    }
    else{
      s.append("  li t0, " + to_string(info.stack_size) + "\n");
      s.append("  add sp, sp, t0\n");
    }
    s.append("  ret\n");
    if(debug){
        cout<<"raw_return return.\n";
    }
    return R_code(s);
}
//integer
R_code Visit(const koopa_raw_integer_t& integer, const R_Info& info){
    if(debug){
        cout<<"raw_integer start.\n";
    }
    string val = to_string(integer.value);
    string s = "  li t0, " + val + "\n";
    if(debug){
        cout<<"raw_integer return.\n";
    }
    s.append(store_reg("t0",info.stack_bio,info));
    return R_code(s,info.stack_bio,1);
}
// 访问对应类型指令的函数定义略
// 视需求自行实现
// ...

R_code Visit(const koopa_raw_jump_t& jump,const R_Info& info){
  string target = riscv_label(jump.target->name);
  string tmp = " j " + target + "\n";
  return R_code(tmp);
}

R_code Visit(const koopa_raw_global_alloc_t& alloc,const R_Info& info){
  if(info.stack_size == 0){
    //cout<<alloc.init->kind.tag<<endl;
    if(debug){
        cout<<"global init happend\n";
    }
    int global_cnt = global_var_cnt;
    global_var_cnt++;
    //cout<<111<<endl;
    if((alloc.init->kind.tag) == KOOPA_RVT_INTEGER){
      if(debug){
        cout<<" int init happend\n";
      }
      int init_value = alloc.init->kind.data.integer.value;
      string val_name = "globalVar"+to_string(global_cnt);
      string tmp = " .data\n .globl "+val_name+"\n"+val_name+":\n  .word "+to_string(init_value)+"\n";
      return R_code(tmp,global_var_id - global_cnt);
    }
    else if((alloc.init->kind.tag) == KOOPA_RVT_ZERO_INIT){
      //cout<<111<<endl;
      if(debug){
        cout<<" zero init happend\n";
      }
      assert(0);
    }
    else{
      //cout<<111<<endl;
      if(debug){
        cout<<" unknown init happend\n";
      }
      assert(0);
    }
  }
  else{
    if(debug){
        cout<<" not global var happend\n";
      }
    string tmp = "";
    tmp += "  li t0, " + to_string(info.stack_bio + 4) + "\n";
    tmp += "  add t0, t0, sp\n";
    tmp += store_reg("t0",info.stack_bio,info);
    return R_code(tmp,info.stack_bio);
    //assert(0);
  }
}

R_code Visit(const koopa_raw_store_t& store,const R_Info& info){
  R_code val = Visit(store.value,info);
  //cout<<info.stack_bio<<endl;
  int src_addr = val.retAddr;
  string tmp = "";
  if(val.append != 0){
    tmp = val.code;
  }

  R_Info nextInfo = info;
  nextInfo.stack_bio = info.stack_bio + 4;
  int dst_addr = Visit(store.dest,nextInfo).retAddr;

  cout<< src_addr << " " << dst_addr<<endl;
  //cout<<info.stack_bio<<endl;

  tmp += load_reg("t1",dst_addr,info);
  tmp += load_reg("t0",src_addr,info);
  tmp += "  sw t0, 0(t1)\n";
  return R_code(tmp);
}

R_code Visit(const koopa_raw_load_t& load,const R_Info& info){
  int src_addr = Visit(load.src,info).retAddr;
  string tmp = load_reg("t0",src_addr,info);
  tmp += "  lw t0, 0(t0)\n";
  tmp += store_reg("t0",info.stack_bio,info);
  return R_code(tmp,info.stack_bio);
}

R_code Visit(const koopa_raw_branch_t& branch,const R_Info& info){
  R_code condition = Visit(branch.cond,info);
  int condition_addr = condition.retAddr;
  string tmp = "";
  if(condition.append != 0){
    tmp = condition.code;
  }
  tmp += load_reg("t0",condition_addr,info);
  string true_label = riscv_label(branch.true_bb->name);
  string false_label = riscv_label(branch.false_bb->name);

  tmp += "  beqz t0, " + false_label + "\n";
  tmp += "  j " + true_label + "\n";
  return R_code(tmp);
}

R_code Visit(const koopa_raw_binary_t& binary,const R_Info& info){
  R_Info left_info = info;
  R_code left_ret = Visit(binary.lhs,left_info);
  string left_code = "";
  if(left_ret.append!=0) left_code = left_ret.code;

  R_Info right_info = info;
  right_info.stack_bio += 4;
  R_code right_ret = Visit(binary.rhs,right_info);
  string right_code = "";
  if(right_ret.append!=0) right_code = right_ret.code;

  string tmp = left_code + right_code;
  tmp += load_reg("t0",left_ret.retAddr,info);
  tmp += load_reg("t1",right_ret.retAddr,info);

  int op = binary.op;
  string op_code = "";
  if(op == KOOPA_RBO_ADD){
    op_code = "  add t0, t0, t1\n";
    tmp.append(op_code);
  }
  else if(op == KOOPA_RBO_SUB){
    op_code = "  sub t0, t0, t1\n";
    tmp.append(op_code);
  }
  else if(op == KOOPA_RBO_MUL){
    op_code = "  mul t0, t0, t1\n";
    tmp.append(op_code);
  }
  else if(op == KOOPA_RBO_DIV){
    op_code = "  div t0, t0, t1\n";
    tmp.append(op_code);
  }
  else if(op == KOOPA_RBO_MOD){
    op_code = "  rem t0, t0, t1\n";
    tmp.append(op_code);
  }
  else if(op == KOOPA_RBO_EQ){
    op_code = "  xor t0, t0, t1\n";
    op_code.append("  seqz t0, t0\n");
    tmp.append(op_code);
  }
  else if(op == KOOPA_RBO_NOT_EQ){
    op_code = "  xor t0, t0, t1\n";
    op_code.append("  snez t0, t0\n");
    tmp.append(op_code);
  }
  else if(op == KOOPA_RBO_LT){
    op_code = "  slt t0, t0, t1\n";
    tmp.append(op_code);
  }
  else if(op == KOOPA_RBO_GT){
    op_code = "  sgt t0, t0, t1\n";
    tmp.append(op_code);
  }
  else if(op == KOOPA_RBO_LE){
    op_code = "  sgt t0, t0, t1\n";
    op_code.append("  seqz t0, t0\n");
    tmp.append(op_code);
  }
  else if(op == KOOPA_RBO_GE){
    op_code = "  slt t0, t0, t1\n";
    op_code.append("  seqz t0, t0\n");
    tmp.append(op_code);
  }
  else if(op == KOOPA_RBO_AND){
    op_code = "  and t0, t0, t1\n";
    tmp.append(op_code);
  }
  else if(op == KOOPA_RBO_OR){
    op_code = "  or t0, t0, t1\n";
    tmp.append(op_code);
  }
  else{
    if(debug){
      cout << "invalid op!\n";
      assert(0);
    }
  }

  tmp += store_reg("t0",info.stack_bio,info);
  return R_code(tmp,info.stack_bio);
}

string KIR_RISCV(const char* str){
    // 解析字符串 str, 得到 Koopa IR 程序
    koopa_program_t program;
    koopa_error_code_t ret = koopa_parse_from_string(str, &program);
    assert(ret == KOOPA_EC_SUCCESS);  // 确保解析时没有出错
    // 创建一个 raw program builder, 用来构建 raw program
    koopa_raw_program_builder_t builder = koopa_new_raw_program_builder();
    if(debug){
      cout<< "raw program build sucess\n";
    }
    // 将 Koopa IR 程序转换为 raw program
    koopa_raw_program_t raw = koopa_build_raw_program(builder, program);
    if(debug){
      cout<< "raw program translate sucess\n";
    }
    // 释放 Koopa IR 程序占用的内存
    koopa_delete_program(program);

    // 处理 raw program
    // ...
    R_code riscv = Visit(raw);
    if(debug){
      cout<< "visit sucess\n";
    }

    // 处理完成, 释放 raw program builder 占用的内存
    // 注意, raw program 中所有的指针指向的内存均为 raw program builder 的内存
    // 所以不要在 raw program 处理完毕之前释放 builder
    koopa_delete_raw_program_builder(builder);
    return riscv.code;

}


